import { Component, OnInit } from '@angular/core';
import { rawData } from './raw-data';
import { AbstractControl, FormArray, FormControl, FormGroup } from '@angular/forms';
import { UserService } from './user.service';

export interface RawData {
  name: string,
  email: string,
  role: string
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})
export class AppComponent implements OnInit {

  private rawData: RawData[];

  userForm = new FormGroup({
    users: new FormArray([])
  });

  constructor(private user: UserService) {
  }

  ngOnInit() {
    this.loadRawData();
  }

  get users() {
    return this.userForm.get('users') as FormArray;
  }

  private loadRawData() {
    // get the rawData from the provided file
    this.rawData = rawData;
    this.createUserForm();
  }

  private createUserForm() {
    // gelijk data omzetten naar een formulier
    this.rawData.map(
      user => new FormGroup({
        id: new FormControl(''),
        name: new FormControl(user.name),
        email: new FormControl(user.email),
        role: new FormControl(user.role),
        status: new FormControl()
      })
    ).forEach(
      (control) => this.users.push(control)
    );
  }

  clickSubmit() {
    // vanuit het sturen we de data weer op
    this.user.saveUsers(this.users.value)
      .subscribe(
        (result) => this.users.patchValue(result)
      );
  }

  toonText(dataRow: AbstractControl) {
    return (!dataRow.value.status || dataRow.value.status === 'OK')
  }
}
