import { Injectable } from '@angular/core';
import { forkJoin, Observable, of, throwError } from 'rxjs';
import { catchError, map, mapTo, switchMap, withLatestFrom } from 'rxjs/operators';
import { MockApiService, Portfolio, Role, RoleTypeEnum, User } from './mock-api.service';
import { RawData } from './app.component';

export interface ProccessableData extends RawData {
  id?: string;
}

export interface ProcessedUser extends User {
  status?: string;
  role?: string,
  portfolio?: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private mockApi: MockApiService) {
  }

  public saveUsers(users) {
    const calls = this.createUserCalls(users);
    return forkJoin(calls)
      .pipe(
        map((results: ProcessedUser[]) => {
            return this.updateStatus(users, results);
          }
        )
      )
  }

  private updateStatus(users, results: ProcessedUser[]) {
    users.forEach(
      u => {
        const result = results.find(r => r.email === u.email);
        if (result) {
          u.status = result.status;
          u.id = result.id;
        }
        return u
      }
    );
    return users;
  }

  private createUserCalls(users) {
    return users
      // .map(user => ({ ...user })) // detach nodig omdat ik later in memory de status update
      .filter(user => user.status !== 'OK')
      .map(user => this.createUser(user));
  }

  private createUser(dataRow: ProccessableData): Observable<ProcessedUser> {
    return this.getOrCreateUser(dataRow)
      .pipe(
        map(user => ({ ...user, status: 'OK' })),
        switchMap(user =>
          forkJoin([
            this.createRole(user, dataRow),
            this.createPortfolio(user, dataRow)
          ])
            .pipe(
              map(([ role, portfolio ]) => ({
                ...user,
                role,
                portfolio
              })),
              catchError(error => of(error)),
            )
        ),
        catchError(error => of(error))
      );
  }

  private getOrCreateUser(dataRow: ProccessableData) {
    return dataRow.id
      ? of(dataRow as User)
      : this.mockApi.createUser(dataRow.name, dataRow.email)
        .pipe(
          catchError((error) =>
            throwError({
              id: '',
              ...dataRow,
              status: error
            })
          )
        );
  }

  private createRole(user: User, dataRow: RawData) {
    return this.mockApi.createRole(user.id, RoleTypeEnum[ dataRow.role.toLowerCase() ])
      .pipe(
        catchError((error) =>
          throwError({
            ...user,
            status: error
          })
        )
      );
  }

  private createPortfolio(user: User, dataRow: RawData) {
    return dataRow.role === RoleTypeEnum.student
      ? this.mockApi.createPortfolio(user.id)
      : of(null)
  }
}
